package com.example.tictactoe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button[] buttons;
    String turn = "X";
    int actions = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttons = new Button[9];

        buttons[0] = (Button) findViewById(R.id.button1);
        buttons[1] = (Button) findViewById(R.id.button2);
        buttons[2] = (Button) findViewById(R.id.button3);
        buttons[3] = (Button) findViewById(R.id.button4);
        buttons[4] = (Button) findViewById(R.id.button5);
        buttons[5] = (Button) findViewById(R.id.button6);
        buttons[6] = (Button) findViewById(R.id.button7);
        buttons[7] = (Button) findViewById(R.id.button8);
        buttons[8] = (Button) findViewById(R.id.button9);
    }

    public void clearButton(View view) {
        TextView tv = (TextView) findViewById(R.id.winnerText);
        tv.setText(R.string.winner_text);

        this.turn = "X";
        this.actions = 0;

        for (int i = 0; i < this.buttons.length; i++) {
            buttons[i].setText("");
            buttons[i].setEnabled(true);
        }
    }

    public void clickActionButton(View view) {
        Button btn = (Button) view;

        if (!btn.isEnabled()) return;

        btn.setText(this.turn);
        btn.setEnabled(false);
        this.actions += 1;

        if (this.checkResult()) {
            TextView tv = (TextView) findViewById(R.id.winnerText);
            tv.setText(this.turn + " won");

            for (int i = 0; i < this.buttons.length; i++) {
                buttons[i].setEnabled(false);
            }
        }

        this.turn = this.turn.equals("X") ? "O" : "X";

        if (this.actions == 9) {
            //end of possible moves
            //no winner

            TextView tv = (TextView) findViewById(R.id.winnerText);
            tv.setText(R.string.no_winner);
        }
    }

    /**
     * checks if there is a winner or not
     *
     * @return bool
     */
    public boolean checkResult() {
        //combinations to win
        boolean hasWinner = false;
        int[][] combinations = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {1, 4, 7},
                {2, 5, 8},
                {3, 6, 9},
                {1, 5, 9},
                {3, 5, 7},
        };

        for (int i = 0; i < combinations.length; i++){
            String player = (String) this.buttons[combinations[i][0]-1].getText();
            int counter = 0;

            for (int j = 0; j < combinations[i].length; j++){
                int btn = combinations[i][j] - 1;
                String current = (String) this.buttons[btn].getText();

                if(current.equals("")) break;

                if (!player.equals(current)) {
                    hasWinner = false;
                    break;
                }
                else {
                    counter += 1;
                    hasWinner = true;
                }
            }

            if(hasWinner && counter == 3) break;
            else hasWinner = false;
        }

        return hasWinner;
    }
}